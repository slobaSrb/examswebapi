﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ExamWebApp.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }
        public string? IndexNo { get; set; }
        public bool IsProfessor { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        //[ForeignKey("Chat")]
        //public int ChatID { get; set; }
        [ForeignKey("ChatID")]
        public ICollection<Chat> Chats { get; set; }
        [ForeignKey("ExamID")]
        public ICollection<Exam> Exams { get; set; }
        [ForeignKey("CheckInID")]
        public ICollection<CheckIn> CheckIns { get; set; }
    }

}