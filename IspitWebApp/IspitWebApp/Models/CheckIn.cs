﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ExamWebApp.Models
{
    public class CheckIn
    {
        [Key]
        public int CheckInID { get; set; }
        public DateTime CheckInDateTime { get; set; }
        public DateTime? CheckOutDateTime { get; set; }
        [ForeignKey("Exam")]
        public int ExamID { get; set; }
        public Exam Exam { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        public User User { get; set; }
        public string? FilePath { get; set; }

    }

}