﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ExamWebApp.Models
{
    public class Chat
    {
        [Key]
        public int ChatID { get; set; }
        public string Message { get; set; }
        [ForeignKey("Exam")]
        public int ExamID { get; set; }
        public Exam Exam { get; set; }
        [ForeignKey("User")]
        public int UserID { get; set; }
        public User User { get; set; }
        public DateTime DateShown { get; set; }

    }

}