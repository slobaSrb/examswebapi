﻿using ExamWebApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ExamWebApp.DAL
{
    public class ExamDbContext:DbContext
    {
        public ExamDbContext():base("ExamDbContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Exam> Exams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<CheckIn> CheckIns { get; set; }
        public DbSet<Chat> Chats { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<User>()
                .HasMany<Exam>(s => s.Exams)
                .WithMany(c => c.Users)
                .Map(cs =>
                {
                    cs.MapLeftKey("UserRefId");
                    cs.MapRightKey("ExamRefId");
                    cs.ToTable("UserExam");
                });
        }

    }
}