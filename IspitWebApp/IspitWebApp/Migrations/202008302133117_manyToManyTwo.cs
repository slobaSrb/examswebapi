namespace ExamWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class manyToManyTwo : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.UserExam", name: "User_UserID", newName: "UserRefId");
            RenameColumn(table: "dbo.UserExam", name: "Exam_ExamID", newName: "ExamRefId");
            RenameIndex(table: "dbo.UserExam", name: "IX_User_UserID", newName: "IX_UserRefId");
            RenameIndex(table: "dbo.UserExam", name: "IX_Exam_ExamID", newName: "IX_ExamRefId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.UserExam", name: "IX_ExamRefId", newName: "IX_Exam_ExamID");
            RenameIndex(table: "dbo.UserExam", name: "IX_UserRefId", newName: "IX_User_UserID");
            RenameColumn(table: "dbo.UserExam", name: "ExamRefId", newName: "Exam_ExamID");
            RenameColumn(table: "dbo.UserExam", name: "UserRefId", newName: "User_UserID");
        }
    }
}
