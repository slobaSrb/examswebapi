namespace ExamWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullFieldsInCheckIn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CheckIn", "CheckOutDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CheckIn", "CheckOutDateTime", c => c.DateTime(nullable: false));
        }
    }
}
