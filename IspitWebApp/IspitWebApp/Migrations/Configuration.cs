namespace ExamWebApp.Migrations
{
    using ExamWebApp.DAL;
    using ExamWebApp.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ExamWebApp.DAL.ExamDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        //protected override void Seed(ExamDbContext context)
        //{

        //    var Users = new List<User>
        //    {
        //        new User{FullName="Slobodan Stankovic",Email="slobodan.stankovic@pmf.edu.rs",IndexNo="71RN",IsProfessor=false,Password="slobodan123",Title="Student of MSc"},
        //        new User{FullName="Djordje Marinkovic",Email="djordje.marinkovic@pmf.edu.rs",IndexNo="63RN",IsProfessor=false,Password="slobodan123",Title="Student of MSc"},
        //        new User{FullName="Nikola Djordjevic",Email="nikola.djordjevic@pmf.edu.rs",IndexNo="73RN",IsProfessor=false,Password="slobodan123",Title="Student of MSc"},
        //        new User{FullName="Marko Petkovic",Email="marko.petkovic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"},
        //        new User{FullName="Marko Milosevic",Email="marko.milosevic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"},
        //        new User{FullName="Svetozar Rancic",Email="svetozar.rancic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"},
        //        new User{FullName="Jelena Ignjatovic",Email="jelena.ignjatovic@pmf.edu.rs",IsProfessor=true,Password="slobodan123",Title="Full Professor"}

        //    };
        //    Users.ForEach(u => context.Users.Add(u));
        //    var Exams = new List<Exam>
        //    {
        //        new Exam{Department="Razvoj Softvera",ExamName="Teorija Programskih Jezika",ExamImagePath="/images/professor_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Razvoj Veb Aplikacija",ExamImagePath="/images/arrival_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Razvoj Mobilnih Aplikacija",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=2 },
        //        new Exam{Department="Upravljanje Informacijama",ExamName="Teorija Informacija i Kodiranje",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Testiranje i Metrika Softvera",ExamImagePath="/images/professor_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Dizajn Softvera",ExamImagePath="/images/arrival_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},
        //        new Exam{Department="Razvoj Softvera",ExamName="Teorija Algoritama Automata i Jezika",ExamImagePath="/images/chat_logo.png",Semester=2,LevelOfStudies="Master",YearOfStudies=1 },
        //        new Exam{Department="Upravljanje Informacijama",ExamName="Kriptografski Algoritmi",ExamImagePath="/images/student_logo.png",Semester=1,LevelOfStudies="Master",YearOfStudies=1},


        //    };
        //    Exams.ForEach(e => context.Exams.Add(e));
        //    context.SaveChanges();
        //}
    }
}
