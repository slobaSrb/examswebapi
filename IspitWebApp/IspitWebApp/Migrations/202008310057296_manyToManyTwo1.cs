namespace ExamWebApp.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class manyToManyTwo1 : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.UserExam",
                c => new
                    {
                        UserRefId = c.Int(nullable: false),
                        ExamRefId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DatabaseGenerated",
                        new AnnotationValues(oldValue: null, newValue: "None")
                    },
                });
            
        }
        
        public override void Down()
        {
            AlterTableAnnotations(
                "dbo.UserExam",
                c => new
                    {
                        UserRefId = c.Int(nullable: false),
                        ExamRefId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DatabaseGenerated",
                        new AnnotationValues(oldValue: "None", newValue: null)
                    },
                });
            
        }
    }
}
