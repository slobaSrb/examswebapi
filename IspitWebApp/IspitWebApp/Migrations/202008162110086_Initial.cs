namespace ExamWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chat",
                c => new
                    {
                        ChatID = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        ExamID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        DateShown = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ChatID)
                .ForeignKey("dbo.Exam", t => t.ExamID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.ExamID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Exam",
                c => new
                    {
                        ExamID = c.Int(nullable: false, identity: true),
                        ExamName = c.String(),
                        Department = c.String(),
                        YearOfStudies = c.Int(nullable: false),
                        Semester = c.Int(nullable: false),
                        LevelOfStudies = c.String(),
                        ExamImagePath = c.String(),
                        ProfessorIdentities = c.String(),
                    })
                .PrimaryKey(t => t.ExamID);
            
            CreateTable(
                "dbo.CheckIn",
                c => new
                    {
                        CheckInID = c.Int(nullable: false, identity: true),
                        CheckInDateTime = c.DateTime(nullable: false),
                        CheckOutDateTime = c.DateTime(nullable: false),
                        ExamID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        FilePath = c.String(),
                    })
                .PrimaryKey(t => t.CheckInID)
                .ForeignKey("dbo.Exam", t => t.ExamID, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.ExamID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        IndexNo = c.String(),
                        IsProfessor = c.Boolean(nullable: false),
                        Email = c.String(),
                        Password = c.String(),
                        FullName = c.String(),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.UserExam",
                c => new
                    {
                        User_UserID = c.Int(nullable: false),
                        Exam_ExamID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserID, t.Exam_ExamID })
                .ForeignKey("dbo.User", t => t.User_UserID, cascadeDelete: true)
                .ForeignKey("dbo.Exam", t => t.Exam_ExamID, cascadeDelete: true)
                .Index(t => t.User_UserID)
                .Index(t => t.Exam_ExamID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Chat", "UserID", "dbo.User");
            DropForeignKey("dbo.Chat", "ExamID", "dbo.Exam");
            DropForeignKey("dbo.CheckIn", "UserID", "dbo.User");
            DropForeignKey("dbo.UserExam", "Exam_ExamID", "dbo.Exam");
            DropForeignKey("dbo.UserExam", "User_UserID", "dbo.User");
            DropForeignKey("dbo.CheckIn", "ExamID", "dbo.Exam");
            DropIndex("dbo.UserExam", new[] { "Exam_ExamID" });
            DropIndex("dbo.UserExam", new[] { "User_UserID" });
            DropIndex("dbo.CheckIn", new[] { "UserID" });
            DropIndex("dbo.CheckIn", new[] { "ExamID" });
            DropIndex("dbo.Chat", new[] { "UserID" });
            DropIndex("dbo.Chat", new[] { "ExamID" });
            DropTable("dbo.UserExam");
            DropTable("dbo.User");
            DropTable("dbo.CheckIn");
            DropTable("dbo.Exam");
            DropTable("dbo.Chat");
        }
    }
}
