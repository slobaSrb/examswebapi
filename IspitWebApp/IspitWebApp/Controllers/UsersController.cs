﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ExamWebApp.DAL;
using ExamWebApp.Models;

namespace IspitWebApp.Controllers
{
    public class UsersController : ApiController
    {
        private ExamDbContext db = new ExamDbContext();

        // GET: api/Users
        public IHttpActionResult GetUsers()
        {
            return Ok(db.Users.ToList<User>());
        }

        // GET: api/Users/GetStudentsForExam/1
        [HttpGet]
        public IHttpActionResult GetStudentsForExam(int id)
        {
            List<User> rezUsers = new List<User>();/// = db.Users.SingleOrDefault(u => u.UserID==0);
            try
            {
                var users = db.Users.Include("Exams").ToList<User>();

                for (int i = 0; i < users.Count(); i++)
                {
                    bool contains = false;
                    for (int j = 0; j < users.ElementAt(i).Exams.Count; j++)
                    {
                        if (users.ElementAt(i).Exams.ElementAt(j).ExamID == id)
                        {
                            contains = true;
                        }
                    }
                    if (contains)
                    {
                        rezUsers.Add(users.ElementAt(i));
                        if (users.ElementAt(i).Exams != null)
                            users.ElementAt(i).Exams.Clear();
                        if (users.ElementAt(i).Chats != null)
                            users.ElementAt(i).Chats.Clear();
                        if (users.ElementAt(i).CheckIns != null)
                            users.ElementAt(i).CheckIns.Clear();
                    }
                }

            }
            catch (Exception e1)
            {
                return NotFound();
            }
            return Ok(rezUsers);
        }
        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(user);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = user.UserID }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.UserID == id) > 0;
        }
    }
}