﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ExamWebApp.DAL;
using ExamWebApp.Models;
using Newtonsoft.Json;

namespace ExamWebApp.Controllers
{
    public class ExamsController : ApiController
    {
        private ExamDbContext db = new ExamDbContext();

        // GET: api/Exams
        public IHttpActionResult GetExams()
        {
            return Ok(db.Exams.ToList<Exam>());
        }

        // GET: api/Exams/5
        [ResponseType(typeof(Exam))]
        public IHttpActionResult GetExam(int id)
        {
            Exam exam = db.Exams.Find(id);
            if (exam == null)
            {
                return NotFound();
            }

            return Ok(exam);
        }

        // PUT: api/Exams/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult PutExam(int id, Exam exam)
        {
            //Exam exam = JsonConvert.DeserializeObject(examStr) as Exam;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exam.ExamID)
            {
                return BadRequest();
            }
            User user = exam.Users.ElementAt(0);
            var model = db.Exams.Include("Users").SingleOrDefault(e => e.ExamID == exam.ExamID);
            var subUser = db.Users.SingleOrDefault(u => u.UserID == user.UserID);
            model.Users.Add(subUser);


            //if(exam.Chats.Count == 1){
            //    ChatsController chat = new ChatsController();
            //    return chat.PostChat(exam.Chats.ElementAt(0));
            //}
            //else if(exam.CheckIns.Count == 1)
            //{
            //    CheckInsController checkin = new CheckInsController();
            //    return checkin.PostCheckIn(exam.CheckIns.ElementAt(0));
            //} else if(exam.Users.Count == 1)
            //{

            //}
            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExamExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.OK);
        }


        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult UnEnroll(int id, [FromBody] string buttonName)
        {
            //Exam exam = JsonConvert.DeserializeObject(examStr) as Exam;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //if (id != exam.ExamID)
            //{
            //    return BadRequest();
            //}

            int userID = int.Parse(buttonName.Split('_')[1]);
            //User user = exam.Users.ElementAt(0);
            var model = db.Exams.Include("Users").SingleOrDefault(e => e.ExamID == id);
            var subUser = db.Users.SingleOrDefault(u => u.UserID == userID);
            model.Users.Remove(subUser);

            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExamExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.OK);
        }
        [ResponseType(typeof(CheckIn))]
        [HttpPut]
        public IHttpActionResult PutCheckIn(int id, [FromBody]  User student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var exam = db.Exams.Include("CheckIns").Where(e => e.ExamID == id).First();
            //List<CheckIn> checkIns = exam.CheckIns.ToList<CheckIn>();
            //exam.CheckIns.Clear();
            var checkIn = new CheckIn();
            DateTime dateValue = DateTime.Now;
            DateTime parseData;
            string pattern = "YYYY-MM-DDTHH:mm:ss";
            try
            {
                if (DateTime.TryParseExact(dateValue.ToString(pattern), pattern, null, DateTimeStyles.None, out parseData))
                {
                    checkIn.CheckInDateTime = parseData;
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e1)
            {
                Console.WriteLine(e1);
            }
            checkIn.ExamID = id;
            checkIn.UserID = student.UserID;
            exam.CheckIns.Add(checkIn);
            db.Exams.Add(exam);
            db.Entry(exam).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExamExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            checkIn = db.CheckIns.SingleOrDefault(c => c.CheckInDateTime == checkIn.CheckInDateTime);
            if (checkIn.Exam != null) checkIn.Exam.CheckIns.Clear();
            if (checkIn.User != null) checkIn.User.CheckIns.Clear();
            return Ok(checkIn);
            // return CreatedAtRoute("DefaultApi", new { id = exam.ExamID }, exam);
        }
        // POST: api/Exams
        [ResponseType(typeof(Exam))]
        public IHttpActionResult PostExam(Exam exam)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Exams.Add(exam);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = exam.ExamID }, exam);
        }

        // DELETE: api/Exams/5
        [ResponseType(typeof(Exam))]
        public IHttpActionResult DeleteExam(int id)
        {
            Exam exam = db.Exams.Find(id);
            if (exam == null)
            {
                return NotFound();
            }

            db.Exams.Remove(exam);
            db.SaveChanges();

            return Ok(exam);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExamExists(int id)
        {
            return db.Exams.Count(e => e.ExamID == id) > 0;
        }
    }
}